<?php

elgg_register_event_handler('init', 'system', 'moviepartner_init');

elgg_register_event_handler('pagesetup', 'system', 'moviepartner_topbar_menu', 0);

function moviepartner_init() {

	/*
	 if (false !== strpos($_SERVER['REQUEST_URI'], '/action/') && ! empty($_SERVER['CONTENT_LENGTH']) && empty($_POST)) {
	 register_error('File too large. It must be below 10mb. Please try another');
	 forward(REFERER);
	 }
	 */
	//elgg_extend_view('page/elements/head', 'page/elements/head_lp');
	elgg_unextend_view('page/elements/header', 'search/header');
	//load javascript
	elgg_extend_view('js/elgg', 'js/toggle_menu');
	elgg_register_simplecache_view('toggle_menu');
	$url = elgg_get_simplecache_url('js', 'toggle_menu');
	elgg_register_js('toggle_menu', $url);
	//elgg_load_js('toggle_menu');

	elgg_unregister_menu_item('topbar', 'elgg_logo');

	elgg_extend_view('js/elgg', 'default/profile/el_js');


	elgg_unregister_plugin_hook_handler('output:before', 'layout', 'elgg_views_add_rss_link');
	// Replace the default register page
	//register_plugin_hook('register', 'system', 'bp_register');
	//elgg_register_page_handler('register','bp_register');

}

/**
 * Sets up user-related menu items
 *
 * @return void
 * @access private
 */
function moviepartner_topbar_menu() {

	$owner = elgg_get_page_owner_entity();
	$viewer = elgg_get_logged_in_user_entity();
	$dropmenu = array();
	$iconURL = elgg_get_site_url() . "_graphics/icons/MPTheme/";
	/*if ($owner) {
	 $params = array(
	 'name' => 'friends',
	 'text' => elgg_echo('friends'),
	 'href' => 'friends/' . $owner->username,
	 'contexts' => array('friends')
	 );
	 elgg_register_menu_item('page', $params);

	 $params = array(
	 'name' => 'friends:of',
	 'text' => elgg_echo('friends:of'),
	 'href' => 'friendsof/' . $owner->username,
	 'contexts' => array('friends')
	 );
	 elgg_register_menu_item('page', $params);

	 elgg_register_menu_item('page', array(
	 'name' => 'edit_avatar',
	 'href' => "avatar/edit/{$owner->username}",
	 'text' => elgg_echo('avatar:edit'),
	 'contexts' => array('profile_edit'),
	 ));

	 elgg_register_menu_item('page', array(
	 'name' => 'edit_profile',
	 'href' => "profile/{$owner->username}/edit",
	 'text' => elgg_echo('profile:edit'),
	 'contexts' => array('profile_edit'),
	 ));
	 }
	 */
	// topbar
	if ($viewer) {
		//User profile
		elgg_register_menu_item('moviepartner_top', 
		array('name' => 'profile', 'href' => $viewer -> getURL(), 
		'text' => 
		elgg_view('output/img', 
		array('src' => $viewer -> getIconURL('topbar'), 
		'alt' => $viewer -> name . '\'s '. elgg_echo('profile-alt'), 'title' => $viewer -> name . '\'s '. elgg_echo('profile'), 
		'class' => 'mp_topbar_avaster', )) . ' ' . 
		$viewer -> name, 
		'priority' => 100, 'link_class' => 'elgg-topbar-avatar', 
		'section' => 'a_menu bg-yellow', ));
		
		//Project icon on topbar
		elgg_register_menu_item('moviepartner_top', 
		array('name' => 'projectPage', 'href' => 'projects/all', 
		'text' => elgg_view('output/img', 
		array('src' => $iconURL . 'add_new_project.png', 'alt' => elgg_echo('mp:project-alt'), 
		'title' => elgg_echo('mp:project'), )), 'priority' => 201, 'section' => 'a_menu bg-yellow', ));
		
		//Blog post icon on topbar
		elgg_register_menu_item('moviepartner_top', 
		array('name' => 'blogPage', 'href' => 'post/all', 
		'text' => elgg_view('output/img', 
		array('src' => $iconURL . 'post.png', 'alt' => elgg_echo('mp:blog-alt'), 
		'title' => elgg_echo('mp:blog'), )), 'priority' => 202, 'section' => 'a_menu bg-yellow', ));
	
		elgg_register_menu_item('moviepartner_top', 
		array('name' => 'toggle', 'href' => "#", 
		'text' => elgg_echo('<div id="moviepartner-toggle-button">
		<a href="#" data-bind-action="toggle-content" 
		data-bind-target="moviepartner-drop-menu-wrapper">' . elgg_view('output/img', 
		array('src' => $iconURL . 'controlPanel.png', 
		'alt' => elgg_echo('setting-alt'), 'title' => elgg_echo('setting'), )) . '</a></div>'), 
		'is_action' => TRUE, 'priority' => 500, 'section' => 'a_menu-after', ));
	
		//$dropmenu[100] = 'profile';
		/*
		 elgg_register_menu_item('moviepartner_top', array(
		 'name' => 'friends',
		 'href' => "friends/{$viewer->username}",
		 'text' => elgg_view_icon('users'),
		 'title' => elgg_echo('friends'),
		 'priority' => 300,
		 'section' => 'a_menu',
		 ));

		 elgg_register_menu_item('moviepartner_top', array(
		 'name' => 'usersettings',
		 'href' => "#",
		 'text' => elgg_view_icon('settings') ,
		 'priority' => 500,
		 'section' => 'settings',
		 ));
		 */
		 
		/*elgg_register_menu_item('moviepartner_top', array(
		 'name' => 'logout',
		 'href' => "action/logout",
		 'text' => elgg_echo('logout'),
		 'is_action' => TRUE,
		 'priority' => 1000,
		 'section' => 'settings',
		 ));*/
		elgg_register_menu_item('moviepartner_drop', 
		array('name' => 'usersettings', 'href' => "settings/user/{$viewer->username}", 
		'text' => elgg_echo('setting'), 'priority' => 500, 'section' => 'settings', ));

		elgg_register_menu_item('moviepartner_drop', 
		array('name' => 'logout', 'href' => "action/logout", 
		'text' => elgg_echo('logout'), 'is_action' => TRUE, 
		'priority' => 1000, 'section' => 'settings', ));
/*
		elgg_register_menu_item('moviepartner_drop', 
		array('name' => 'dashboard', 'href' => 'dashboard', 
		'text' => elgg_view_icon('home') . elgg_echo('dashboard'), 
		'priority' => 90, 'section' => 'settings', ));
*/			
		elgg_register_menu_item('moviepartner_drop', 
		array('name' => 'Group', 'href' => 'groups/all', 
		'text' => elgg_view_icon('users') . elgg_echo('group'), 
		'priority' => 10, 'section' => 'settings', ));
		
		elgg_register_menu_item('moviepartner_drop', 
		array('name' => 'Videos', 'href' => 'videos/all', 
		'text' => elgg_view_icon('video') . elgg_echo('video'), 
		'priority' => 1, 'section' => 'settings', ));
	
		if (elgg_is_admin_logged_in()) {
			elgg_register_menu_item('moviepartner_drop', 
			array('name' => 'administration', 'href' => 'admin', 
			'text' => elgg_echo('admin'), 'priority' => 100, 'section' => 'settings', ));
		}
	
	} else {
		//moved to topbar.php
		//$login = elgg_view("core/account/login_dropdown");
		//elgg_register_menu_item('moviepartner_top', array('name' => 'login', 'href' => "javascript:#", 'text' => $login, 'section' => 'a_menu-after', ));

	}
	//Search Bar
	elgg_register_menu_item('moviepartner_top', 
	array('name' => 'search', 'href' => "javascript:#", 
	'text' => elgg_view('search/moviepartner_search_box'),
	
	//'is_action' => TRUE,
	'priority' => 499, 'section' => 'a_menu-after', ));
}

/**
 * Display notification of new messages in topbar

 function messages_notify() {
 if (elgg_is_logged_in()) {
 $iconURL = elgg_get_site_url() . "_graphics/icons/MPTheme/";
 $class = "elgg-icon elgg-icon-mail";
 //$class = "mp-Message";
 $text = elgg_view('output/img', array('src' => $iconURL . 'mail.png', 'alt' => 'Message', 'title' => elgg_echo('Message'), ));
 //"<span class='$class'></span>";
 $drop_text = 'Messages';
 $tooltip = elgg_echo("messages");

 // get unread messages
 $num_messages = (int)messages_count_unread();
 if ($num_messages != 0) {
 $text .= "<span class=\"messages-new\">$num_messages</span>";
 $drop_text .= "<span class=\"messages-new\">$num_messages</span>";
 $tooltip .= " (" . elgg_echo("messages:unreadcount", array($num_messages)) . ")";
 }

 elgg_register_menu_item('moviepartner_top', array('name' => 'messages', 'href' => 'messages/inbox/' .  elgg_get_logged_in_user_entity() -> username, 'text' => $text, 'priority' => 200, 'title' => $tooltip, 'section' => 'a_menu bg-yellow', ));
 }
 }
 */
 /*
function moviepartner_register_dropdown() {

}*/
