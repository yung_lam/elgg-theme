<?php
/**
 * Elgg pageshell
 * The standard HTML page shell that everything else fits into
 *
 * @package Elgg
 * @subpackage Core
 *
 * @uses $vars['title']       The page title
 * @uses $vars['body']        The main content of the page
 * @uses $vars['sysmessages'] A 2d array of various message registers, passed from system_messages()
 */

// backward compatability support for plugins that are not using the new approach
// of routing through admin. See reportedcontent plugin for a simple example.
if (elgg_get_context() == 'admin') {
	if (get_input('handler') != 'admin') {
		elgg_deprecated_notice("admin plugins should route through 'admin'.", 1.8);
	}
	elgg_admin_add_plugin_settings_menu();
	elgg_unregister_css('elgg');
	echo elgg_view('page/admin', $vars);
	return true;
}

// render content before head so that JavaScript and CSS can be loaded. See #4032
$topbar = elgg_view('page/elements/topbar', $vars);
$messages = elgg_view('page/elements/messages', array('object' => $vars['sysmessages']));
$body = elgg_view('page/elements/body', $vars);
$footer = elgg_view('page/elements/footer', $vars);

// add header different if front
if(elgg_get_context()=='front') {
	$header = elgg_view('page/elements/header_front', $vars);
	$header_css = 'livpast-page-header-front';
} else {
	$header = elgg_view('page/elements/header', $vars);
	$header_css = 'livpast-page-header';
}
$lemonbar = elgg_view('page/elements/topbar', $vars);

// Set the content type
header("Content-type: text/html; charset=UTF-8");

?>
<!DOCTYPE HTML><html>
<head>
<?php echo elgg_view('page/elements/head', $vars); ?>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />
	<!--[if !IE 7]>
        <style type="text/css">
            #wrap {display:table;height:100%}
        </style>
    <![endif]-->
</head>
<body>

	<div class="elgg-page-messages">
		<?php echo $messages; ?>
	</div>
	
	<div id="moviepartner-site-wrapper">
		<div id="moviepartner-topbar" class="bg-dark">
			<div class="moviepartner-content-wrapper">
				<div class="moviepartner-content">
					<?php echo $topbar; ?>
				</div>
			</div>
		</div>
		
		
		<div id="moviepartner-main-wrapper" class="moviepartner-wrapper">
			<?php echo $body; ?>
		</div>
		
		
	</div>
	<div id="moviepartner-footer">
		<?php echo $footer; ?>
	</div>		
<?php echo elgg_view('page/elements/foot'); ?>
</body>
</html>