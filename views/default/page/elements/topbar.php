<?php
/**
 * Elgg topbar
 * The standard elgg top toolbar
 */
$MP_url = elgg_get_site_url();
// Elgg logo
$img_url = elgg_get_site_url().'mod/MPtheme/graphics/logo.png';

if (!elgg_is_logged_in()){
	echo elgg_view("core/account/login_dropdown");
}

echo elgg_view_menu('moviepartner_top', array('sort_by' => 'priority', array('elgg-menu-hz')));
echo "<div class='MP_logo'><a href=\"$MP_url\"><img src=\"$img_url\" alt=\"Movie Partner\" /></a></div>";
//echo elgg_view('search/moviepartner_search_box');
//echo '<div class="clearfix"></div>';
//echo elgg_view('input/dropdown', elgg_view_menu('moviepartner_drop', array('sort_by' => 'priority', array('elgg-menu-hz'))));

echo elgg_view('page/elements/drop_menu');

// elgg tools menu
// need to echo this empty view for backward compatibility.
$content = elgg_view("navigation/topbar_tools");
if ($content) {
	elgg_deprecated_notice('navigation/topbar_tools was deprecated. Extend the topbar menus or the page/elements/topbar view directly', 1.8);
	echo $content;
}
