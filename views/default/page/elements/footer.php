<?php
/**
 * Elgg footer
 * The standard HTML footer that displays across the site
 *
 * @package Elgg
 * @subpackage Core
 *
<div id="moviepartner-footer-content" class="moviepartner-wrapper-inside">
	<div id="moviepartner-footer-left" class="moviepartner-content-wrapper">
		<div class="moviepartner-content">
			<?php echo elgg_view('page/elements/footer/footer_left'); ?> 
		</div>
	</div>
	<div id="moviepartner-footer-main" class="moviepartner-content-wrapper">
		<div class="moviepartner-content">
			<?php echo elgg_view('page/elements/footer/footer_main'); ?> 
		</div>
	</div>
	<div id="moviepartner-footer-right" class="moviepartner-content-wrapper">
		<div class="moviepartner-content">
			<?php echo elgg_view('page/elements/footer/footer_right'); ?> 
		</div>
	</div>
	<div class="clearfix"></div>
</div>
 * 
 */

 
 

?>

 <div id="moviepartner-footer-content" class="moviepartner-wrapper-inside">
 
 <?php echo elgg_view('output/longtext', array('value' => elgg_echo("expages:copyright")));?>
 
  <?php echo elgg_view_menu('footer', array(
    'sort_by' => 'priority',
    'class' => 'elgg-menu-hz',
));?> 
</div>
