<?php
/**
 * Gallery view
 *
 * Implemented as an unorder list
 *
 * @uses $vars['tabs']         Array of ElggEntity or ElggAnnotation objects
 * @uses $vars['tab_class'] Additional CSS class for the <ul> element
 * @uses $vars['item_class']    Additional CSS class for the <li> elements
 */

$tabs = $vars['tabs'];
if (!is_array($tabs) || sizeof($tabs) == 0) {
	return true;
}
$tabDiv_class=$vars['tabDiv_class'];
$tab_class = 'css-tabs';
if (isset($vars['tab_class'])) {
	$tab_class = "$tab_class {$vars['tab_class']}";
}

echo "<div class='$tabDiv_class'>";
foreach ($tabs as $id => $items) {
    echo "<ul id='$id' class='$tab_class'>";
    foreach ($items as $item) {
    	echo "<li>";
    	echo $item;
    	echo "</li>";
    }
    echo '</ul>';
}
echo '</div>';
echo "<div id='recruit-Content' class='css-panes'>";
 
echo '</div>';
?>
