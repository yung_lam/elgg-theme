<?php
/**
 * Gallery view
 *
 * Implemented as an unorder list
 *
 * @uses $vars['divID']         div ID
 * @uses $vars['items']         Array of ElggEntity or ElggAnnotation objects
 * @uses $vars['countDiv']         Number of items in the div
 * @uses $vars['gallery_class'] Additional CSS class for the <ul> element
 * @uses $vars['item_class']    Additional CSS class for the <li> elements
 */

$items = $vars['items'];
if (!is_array($items) || sizeof($items) == 0) {
	return true;
}

$limit = $vars['limit'];
$countDiv = $vars['countDiv'];
$itemsCount=0;

$item_class = 'mp-item';
if (isset($vars['item_class'])) {
	$item_class = "$item_class {$vars['item_class']}";
}
$divID=$vars['divID'];

$titleArray=$vars['title'];
if(isset($titleArray)){
    echo "<div class='toptitle'>";
    if($vars['index_title']){
        echo "<span class='font-yellow'>". elgg_echo('mp:top') .' </span>'."<span class='font-dark'>".$titleArray[0].' </span>';
    }else{
         echo $titleArray;
    }
    echo "</div>";
}

echo '<div class="scrollableDIV">';
echo '<a class="prev browse left"></a>';
echo "<div class='scrollable' id='$divID'>";
echo '<div class="items">';

foreach ($items as $item) {
    if($itemsCount==0){
        echo "<div>";
    }
    
    echo "<div class='$item_class'>".$item."</div>";
    $itemsCount++;
    if($itemsCount==$countDiv){
        $itemsCount=0;
        echo "</div>";
    }
    
}
$mod=count($items)%$countDiv;
if($mod!=0){
     echo "</div>";
}
echo '</div>';
echo '</div>';
echo '<a class="next browse right"></a>';
echo '</div>';
?>
