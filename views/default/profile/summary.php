<?php
/**
 * Elgg user display (details)
 * @uses $vars['user'] The user entity
 * @uses $vars['nameclass'] The class for name display
 */

$user = $vars['user'];

$profile_fields = elgg_get_config('profile_fields');
$name="<div class='{$vars['nameclass']}'>$user->name</div>";

$shortname="career";
$items=$user->$shortname;
if(is_array($items)){
    foreach ($items as $key => $value) {
        $career.=$value;
        if ($key<(count($items)-1)) {
            $career.=", ";
        }
    }
}else{
    $career=$items;
}
$career="<div class='{$vars['careerclass']}'>$career</div>";
echo $name.$career;

$options = array(
    'type' => 'group',
    'subtype' => 'project',
    'relationship' => 'member',
    'relationship_guid' => $user->guid,
    'count' => TRUE,
);
$countMembership=elgg_get_entities_from_relationship_count($options);

$options['relationship']='invested';
$countInvested=elgg_get_entities_from_relationship_count($options);

$options['relationship']='donated';
$countDonated=elgg_get_entities_from_relationship_count($options);

$options['relationship']='support';
$countSupport=elgg_get_entities_from_relationship_count($options);

$options['owner_guid']=$user->guid;
$countOwnership=elgg_get_entities($options);
echo "<table class='{$vars['tableclass']}'><tr><td>Created</td><td>$countOwnership</td><td>Project</td></tr>
            <tr><td>Joined</td><td>$countMembership</td><td>Project</td></tr>
            <tr><td>Invested</td><td>$countInvested</td><td>Project</td></tr>
            <tr><td>Donated</td><td>$countDonated</td><td>Project</td></tr>
            <tr><td>Support</td><td>$countSupport</td><td>Project</td></tr>
      </table>";
