<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

form.moviepartner-search {
	width: 205px;
	display: inline-block;
	background-color: #fff;
	
	border-radius: 5px;
	-webkit-border-radius: 5px;
    -moz-border-radius: 5px;
}
.elgg-input-search{
    width:77%;
}
@media screen and (max-width: 1100px) {
	form.moviepartner-search {
		width: 205px;
	}
}

@media screen and (max-width: 800px) {
	form.moviepartner-search {
		width: 205px;
	}
}
	
@media screen and (max-width: 640px) {
	form.moviepartner-search {
		width: 205px;
	}
}

.moviepartner-search-header {
	bottom: 5px;
	height: 23px;
	position: absolute;
	right: 0;
}

.moviepartner-search input[type=submit] {
	position: absolute;
	right: 6px;
	display: inline-block;
	outline: none;
	border: none;
    cursor: pointer;
    text-align: center;
    text-decoration: none;
    font: 14px/100% Arial, Helvetica, sans-serif;
    color: #FFF;
	background-color: #272626;
	width:31px;
	
	text-shadow: 0 1px 1px rgba(0,0,0,.3);
    -webkit-border-radius: .5em; 
    -moz-border-radius: .5em;
    border-radius: .5em;
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
    -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
    box-shadow: 0 1px 2px rgba(0,0,0,.2);
}

.moviepartner-search input[type=submit]:hover {
    text-decoration: none;
}

.moviepartner-search input[type=text] {
	border: none;
	//margin-right: 8px;
	//border: 1px solid #71b9f7;
	//color: white;
	font-size: 12px;
	font-weight: bold;
	//background: transparent url(<?php echo elgg_get_site_url(); ?>_graphics/elgg_sprites.png) no-repeat 2px -934px;
	moz-transition: background 0.5s ease;
	transition: background 0.3s ease;
}
.moviepartner-search input[type=text]:focus, .elgg-search input[type=text]:active {
	background-color: white;
	//background-position: 2px -916px;
	//border: 1px solid white;
	//color: #0054A7;
}

.search-list li {
	padding: 5px 0 0;
}
.search-heading-category {
	margin-top: 20px;
	color: #666666;
}

.search-highlight {
	background-color: #bbdaf7;
}
.search-highlight-color1 {
	background-color: #bbdaf7;
}
.search-highlight-color2 {
	background-color: #A0FFFF;
}
.search-highlight-color3 {
	background-color: #FDFFC3;
}
.search-highlight-color4 {
	background-color: #ccc;
}
.search-highlight-color5 {
	background-color: #4690d6;
}
