<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

/*Opera Fix*/
body:before {
    content:"";
    height:100%;
    float:left;
    width:0;
    margin-top:-32767px;/
}
body {
    margin: 0;
    padding: 0;
    height: 100%;
    font-family:"Times New Roman",Georgia,Serif; 
}

p {
    color: #333;
}

.clearfix {
    float: none;
    clear: both;
}

.comhype-logo img {
    margin-top: -7px;
}

/*************** site wrapper  **************
 * this color will be the footer color
 * **/

div#moviepartner-site-wrapper {
    min-height:100%;
}


div.moviepartner-wrapper {
    display: block;
    padding-top: 60px;
}

div.moviepartner-wrapper-inside {
    display: block;
    width: 900px;
    margin: auto;
	padding: 10px;
    
}

@media screen and (max-width: 1100px) {
    div.moviepartner-wrapper-inside {
        width: 800px
    }
}

@media screen and (max-width: 800px) {
    div.moviepartner-wrapper-inside {
        width: 640px
    }
}
@media screen and (max-width: 640px) {
    div.moviepartner-wrapper-inside {
        width: 100%
    }
    .elgg-col-1of3 {
        display: block;
        float: none;
        width: 100%;
    }
    .profile elgg-col-2of3 {
        display: block;
        float: none;
        width: 100%;
    }
    .profile {
        display: block;
        float: none;
        width: 100%;
    }
    
    div#profile-owner-block {
        display: block;
        float: none;
        width: 100%;
    }
    
    div#profile-details {
        display: block;
        float: none;
        width: 100%;
    }
    
}

div.moviepartner-content-wrapper {
    display: block;
    padding: 0;
    width: 900px;
    margin: auto;
}


div.moviepartner-content {
    display: block;
    //padding: 10px;
}
div.moviepartner-content #login-dropdown{
	position: absolute;
	top:10px;
	right:auto;
	z-index: 100;
}



/*************** TOPBAR ****************/

div#moviepartner-topbar {
    display: block;
    height: 40px;
    //background-color: RGB(24,24,24);
    color: #FFF;
    position: fixed;
    left: 0;
    right: 0;
    top: 0;
    z-index: 300;
    //background-color: #3F3F3F;
    //background-image: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #3F3F3F), color-stop(100%, #000));
    //background-image: -webkit-linear-gradient(top, #3F3F3F 0%,#000 100%);
    //background-image: -moz-linear-gradient(top, #3F3F3F 0%,#000 100%);
    //background-image: -o-linear-gradient(top, #3F3F3F 0%,#000 100%);
    //background-image: linear-gradient(top, #3F3F3F 0%,#000 100%);
}


/*************** HEADER ****************/

div#moviepartner-header {
    display: block;
    background-color: #2A7CD4;
}

div#moviepartner-header-content {

    background-color: #3A8CE4;
}


/*************** MAIN ****************/

div#moviepartner-main-wrapper {
    /* IE10 Consumer Preview */ 
    background-image: -ms-linear-gradient(top, #E8E8E8 0%, #FFFFFF 30%);
    
    /* Mozilla Firefox */ 
    background-image: -moz-linear-gradient(top, #E8E8E8 0%, #FFFFFF 30%);
    
    /* Opera */ 
    background-image: -o-linear-gradient(top, #E8E8E8 0%, #FFFFFF 30%);
    
    /* Webkit (Safari/Chrome 10) */ 
    background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #E8E8E8), color-stop(0.3, #FFFFFF));
    
    /* Webkit (Chrome 11+) */ 
    background-image: -webkit-linear-gradient(top, #E8E8E8 0%, #FFFFFF 30%);
    
    /* W3C Markup, IE10 Release Preview */ 
    background-image: linear-gradient(to bottom, #E8E8E8 0%, #FFFFFF 30%);
    overflow:auto;
    padding-bottom: 150px;/* must be same height as the footer */
   padding-top: 40px;
}

div#moviepartner-main-content {
}

div#moviepartner-column-right {
    width: 150px;
    min-height: 100px;
    padding: 15px;
    position: absolute;
    z-index: 100;
}

div#moviepartner-column-main {
    display: block;
    min-height: 100px;
    position: relative;
    float: left;
    width:100%;
}

@media screen and (max-width: 1100px) {
    div#moviepartner-column-right {
        //width: 200px;
         position: relative;
    }

}

@media screen and (max-width: 800px) {
    div#moviepartner-column-right {
        //width: 200px;
    }
}
    
@media screen and (max-width: 640px) {
    div#moviepartner-column-right {
        width: 100%;
        position: relative;
    }
    div#moviepartner-column-main {
        width: 100%;
    }

}

.mp-block{
    display: inline-table;
    border:#E2DFDF 1px solid;
    padding: 10px;
    margin: 5px 0px;
    box-shadow: inset 1px 1px 19px #E9E9E9;
}

/*************** FOOTER ****************/


div#moviepartner-footer {
	background-color: black;
	width:100%;
	color: #FFF;  
   position: relative;
   margin-top: -150px;
   height: 150px;
   clear: both;
}

div#moviepartner-footer-content .elgg-output{

	text-align: right;
}

div#moviepartner-footer p {
	color: #999;
	display: block;
	margin: 10px auto;
}




/*************** MAIN-MENU ****************/



div.moviepartner-menu-wrapper {
    padding: 5px 0;
}

div#moviepartner-main-menu {
    display: block;
}

div#moviepartner-main-menu ul {
    padding: 0;
    margin: 0;
}

div#moviepartner-main-menu li {
    color: #DDD;
    padding: 0;
    line-height: 28px;
    font-size: 14px;
}

div#moviepartner-main-menu li a{
    text-decoration:none;
    color: #FFF;
}
div#moviepartner-main-menu li a:hover{
    text-shadow: 1px 2px 2px rgba(0,0,0,0.7);
}

div.moviepartner-item {
    float: left;
    padding: 0px 10px;
    margin-right: 3px;
    margin-bottom: 3px;
    text-shadow: 1px 1px 2px rgba(0,0,0,0.4);
    font-size: 12.5px;
}

div.moviepartner-item:hover {
    text-shadow: 1px 2px 2px rgba(0,0,0,0.7);
    text-decoration: underline;
    //border-bottom: 2px solid #FFF;

}


div#moviepartner-main-menu li:hover {
    background: #000;
}



@media screen and (max-width: 480px){
    div#moviepartner-main-menu li {
        display: block;
        line-height: 32px;
        margin-bottom: 5px;
    }
    div#moviepartner-main-menu li:last-child {
        margin-bottom: 0;
    }
}


/**************** modules ******************/

div.elgg-module {
    
}

div.elgg-module div.elgg-head{
    background-color: #FFF;
}

div.elgg-module div.elgg-body{
    background-color: #FFF;
    border-top: 1px solid #DEDEDE;
}

div.moviepartner-content div.elgg-body, div.moviepartner-content div.elgg-head{
    background-color: transparent;
}

li.elgg-menu-item-site-logo {
    
}



div#IE8less {

}


div.MP_logo img{
    display: block;
    margin: auto;
    padding-top: 4px;
}

<?php if (!elgg_is_logged_in()){ ?>
div.MP_logo img{
    display: block;
    margin: auto;
    padding-top: 4px;
    margin-left: 387px;

}
<?php } ?>

.bg-dark{
    background: url(http://preview.ait-themes.com/directory/wp1/wp-content/themes/directory/design/img/search_bg_pattern.png) repeat #2C373F;
border-bottom-width: 1px;
border-bottom-style: solid;
border-bottom-color: #1E5B85;
border-top-width: 1px;
border-top-style: solid;
border-top-color: #0A5C93;
box-shadow: rgba(255, 255, 255, 0.0980392) 0px 1px 0px inset, rgba(0, 0, 0, 0.0470588) 0px -33px 16px -10px inset, rgba(250, 250, 250, 0.0980392) 0px 33px 18px -10px inset, rgba(0, 0, 0, 0.298039) 0px -10px 1px -10px inset;
}

.bg-yellow{
    background: url(http://preview.ait-themes.com/directory/wp1/wp-content/themes/directory/design/img/search_bg_pattern.png) repeat #FFD06C;
box-shadow: rgba(255, 255, 255, 0.0980392) 0px 1px 0px inset, rgba(0, 0, 0, 0.0470588) 0px -33px 16px -10px inset, rgba(250, 250, 250, 0.0980392) 0px 33px 18px -10px inset, rgba(0, 0, 0, 0.298039) 0px -10px 1px -10px inset;
}

.bg-gray{
    background: RGB(160,160,160);
}

.mp-Message{
      background: url(<?php echo $vars['url']; ?>_graphics/icons/MPTheme/mail.png);
}

.percent{
    float: right;
    font-size: 40px;
}
.progressBarDiv{
	height: 1.1em;
	width: 100%;
}
.float-right {
    float:right;
}
.rotate90{
    -webkit-transform: rotate(90deg);   
    -moz-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    -o-transform: rotate(90deg);
    transform: rotate(90deg);
}
.inline-blk {
    display: inline-block;
}

