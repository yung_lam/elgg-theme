<?php

$english = array(
	'mytheme:tools' => 'Tools',
	'mp:partner'=>"Partner",
	'mp:equipment'=>"Equipment",
	'profile:personalinfo'=>"<span class='font-dark'>Personal </span><span class='font-yellow'>Info</span>",
	'pages:project'=>"Project Page",
	'file:project'=>"Project File",
    'remark' => 'Remark',
    
	
	'mp:blog' => 'Blog',
	'mp:blog-alt' => 'Blog icon',
	'mp:project' => 'Project',
	'mp:project-alt' => 'Project icon',
	'profile-alt' => 'Profile icon',
	'setting' => 'Settings',
	'setting-alt' => 'Setting icon',
	
	'mp:top' => 'Top',
);

add_translation('en', $english);
